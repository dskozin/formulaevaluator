<?php
/**
 * Created by PhpStorm.
 * User: dskozin
 * Date: 19.07.18
 * Time: 7:19
 */

namespace Formulas;

class Plus extends AbstractEvaluator
{
//    private $firsArg;
    private $secondArg;

    /**
     * Plus constructor.
     * @param $firsArg
     * @param $secondArg
     */
    public function __construct(array $args)
    {
//        $this->firsArg = $args[0];
        $this->secondArg = $args[0];
    }


    public function getEval($firstArg)
    {
        return $firstArg + $this->secondArg;
    }


}