<?php
/**
 * Created by PhpStorm.
 * User: dskozin
 * Date: 20.07.18
 * Time: 6:56
 */

namespace Formulas;


abstract class AbstractEvaluator implements Evaluation
{

    public function getOrderedEval(array $arguments){
        if (is_numeric($arguments[0])){
            return $this->getEval($arguments);
        }

        return $this->getEval($arguments[0]->getEval($arguments));
    }

}