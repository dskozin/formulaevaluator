<?php

namespace Formulas;

class Formula
{

    private $evals = [
        'Plus'  => Plus::class,
        'Minus' => Minus::class
    ];

    private $formula = [];

    public function addEval($evalName, ...$args){
            $this->formula[] = new $this->evals[$evalName]($args);
    }

    public function evaluateFormula(){
        $result = 0;
        foreach ($this->formula as $formul){
            $result = $formul->getEval($result);
        }
        return $result;
    }

}