<?php

namespace Formulas;

class Minus extends AbstractEvaluator
{
//    private $firstArg;

    private $secondArg;

    /**
     * Minus constructor.
     * @param $firstArg
     * @param $secondArg
     */
    public function __construct(array $args)
    {
//        $this->firstArg = $args[0];
        $this->secondArg = $args[0];
    }


    public function getEval($firstArg)
    {
        return $firstArg - $this->secondArg;
    }


}