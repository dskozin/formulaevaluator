<?php
/**
 * Created by PhpStorm.
 * User: dskozin
 * Date: 19.07.18
 * Time: 7:19
 */

namespace Formulas;

interface Evaluation
{

    public function getOrderedEval(array $arguments);

    public function getEval($firsArg);

}