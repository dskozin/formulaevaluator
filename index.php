<?php

include_once __DIR__ . '/Formula.php';
include_once __DIR__ . '/Evaluation.php';
include_once __DIR__ . '/AbstractEvaluator.php';
include_once __DIR__ . '/Minus.php';
include_once __DIR__ . '/Plus.php';

use Formulas\Formula;
use Formulas\Minus;
use Formulas\Plus;

$formula = new Formula();

$formula->addEval('Plus', 3);
$formula->addEval('Minus',  5);
$formula->addEval('Plus', 3);
$formula->addEval('Minus', 7);
$formula->addEval('Minus', 7);
$formula->addEval('Minus', 7);
var_dump($formula);
echo $formula->evaluateFormula().PHP_EOL;